# Email Templates

This repository contains email templates for various publications within Port Phillip Publishing.

## Getting Started

For a new publication:
Create a folder using allocated pub code.

Save daily and endo templates for new publication following naming convention:
lowercase pub code followed by whether daily or endo and finishing with EmailTemplate.html
i.e. artDailyEmailTemplate.html or artEndoEmailTemplate.html

### Prerequisites

What you will require from the designer:

 - Background colour 
 - Footer colour 
 - Link colour - main content 
 - Link colour - footer
- jpg banner Image 80 x 640 - to be upload to PPP website
- footer image - to be upload to PPP website
- Editor name

Please note: Endo templates have main content links set to blue #0000ff.

### What to update per template

- Title
- background-color and link colours in head
- background-color and bgcolor on tables.
- Link colours in head
- Link colours inline.
- Banner img src and title.
- Footer img src and title.

## Running the tests

All new and approved email template files are to be tested in [250ok](https://250ok.com/) for visual compatibility across various desktop, mobile, tablet and webmail email clients.

Create a new design test, add appropriate subject line and paste email html template code in new window.
Design/Design Tests/New Design Test.

Check through all results for issues and correct and retest as required.

Once happy with testing email proofs to production manager.

Make changes as required, retest and resend revised file to production manager.

Repeat as required.

## Deployment

Once approved, final files to be emailed to production.

## Versioning

We use [Bitbucket](https://bitbucket.org) and [Sourcetree](https://www.sourcetreeapp.com/) for versioning. 
https://bitbucket.org/portphillippublishing/email-templates.git 
